package com.ruoyi.project.lys.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author: author
 * @date: 16:46 2021/7/13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cases extends BaseEntity {
    private String chId; //病例编号
    private String regId;//挂号单ID
    @DateTimeFormat(pattern = "yy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yy-MM-dd HH:mm:ss")
    private Date caseDate; //发病日期
    private String receiveType; //接诊类型
    private String isContagious; //是否传染
    private String caseTitle; //主诉
    private String caseResult;//诊断信息
    private String doctorTips; //医生建议
    private String remarks; //病例备注

}
