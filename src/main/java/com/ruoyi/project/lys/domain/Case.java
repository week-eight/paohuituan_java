package com.ruoyi.project.lys.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author: author
 * @date: 16:46 2021/7/13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Case extends BaseEntity {
    private String id; //病例编号
    @DateTimeFormat(pattern = "yy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yy-MM-dd HH:mm:ss")
    private Date date; //发病日期
    private String type; //接诊类型
    private String infect; //是否传染
    private String chief; //主诉
    private String diagnose;//诊断信息
    private String suggest; //医生建议
    private String remake; //病例备注
}
