package com.ruoyi.project.lys.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: author
 * @date: 18:49 2021/7/13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Treatment extends BaseEntity {
    private String identity; //患者身份证号
    private String caseId;   //病例编号
    private String registrationId; //挂号单ID
}
