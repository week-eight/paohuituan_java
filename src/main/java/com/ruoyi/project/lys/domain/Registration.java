package com.ruoyi.project.lys.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author: author
 * @date: 18:37 2021/7/13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Registration extends BaseEntity {
    private String id; //挂号单ID
    private int patientid; //流水号
    private String name; //患者姓名
    private String depart;//挂号科室
    private String  doctor;//接诊医生
    private Double  money;//挂号费用
    private String  state;//挂号状态
    @DateTimeFormat(pattern = "yy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yy-MM-dd HH:mm:ss")
    private Date   date; //就诊日期
    private String type; //挂号类型
    private String time; //挂号时段

}
