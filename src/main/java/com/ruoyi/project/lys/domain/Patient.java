package com.ruoyi.project.lys.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author: author
 * @date: 13:39 2021/7/13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Patient extends BaseEntity {
    private int id;  //流水号
    private String name;
    private String phone;
    private String identity;  //患者身份证号
    @DateTimeFormat(pattern = "yy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yy-MM-dd HH:mm:ss")
    private Date birth;    //患者出生日期
    private int age;
    private String sex;
    private String state;   //信息状态
    @DateTimeFormat(pattern = "yy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yy-MM-dd HH:mm:ss")
    private Date  start;    //患者创建时间
    private  String address;
    private String allergy;
    private Registration registration;
}
