package com.ruoyi.project.lys.service.impl;

import com.ruoyi.project.lys.domain.Patient;
import com.ruoyi.project.lys.domain.Registration;
import com.ruoyi.project.lys.mapper.RegistrationMapper;
import com.ruoyi.project.lys.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: author
 * @date: 21:19 2021/7/13
 */
@Service
public class RegistrationServiceImpl implements RegistrationService {
    @Autowired
    public RegistrationMapper registrationMapper;
    @Override
    public List<Registration> find(String state) {
        return registrationMapper.selectByState(state);
    }

    @Override
    public int changeState(int id) {
        return registrationMapper.changeState(id);
    }
}
