package com.ruoyi.project.lys.service;


import com.ruoyi.project.lys.domain.Registration;

import java.util.List;

/**
 * @author: author
 * @date: 21:17 2021/7/13
 */
public interface RegistrationService {
    List<Registration>  find(String state);
    int changeState(int id);
}
