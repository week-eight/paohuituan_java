package com.ruoyi.project.lys.service.impl;

import com.ruoyi.project.lys.domain.Patient;
import com.ruoyi.project.lys.mapper.PatientMapper;
import com.ruoyi.project.lys.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: author
 * @date: 14:49 2021/7/15
 */
@Service
public class PatientServiceImpl implements PatientService {
    @Autowired
    private PatientMapper patientMapper;
    @Override
    public Patient findOne(int id) {
        return patientMapper.selectOne(id);
    }
}
