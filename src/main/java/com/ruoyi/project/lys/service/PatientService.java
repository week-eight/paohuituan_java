package com.ruoyi.project.lys.service;

import com.ruoyi.project.lys.domain.Patient;

/**
 * @author: author
 * @date: 14:47 2021/7/15
 */
public interface PatientService {
    //根据流水号回显患者信息
    Patient findOne(int id);
}
