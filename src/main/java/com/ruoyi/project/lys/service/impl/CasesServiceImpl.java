package com.ruoyi.project.lys.service.impl;

import com.ruoyi.project.lys.domain.Cases;
import com.ruoyi.project.lys.mapper.CasesMapper;
import com.ruoyi.project.lys.service.CasesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @author: author
 * @date: 20:43 2021/7/15
 */
@Service
public class CasesServiceImpl implements CasesService {
    @Autowired
    private CasesMapper casesMapper;
    @Override
    public int insertCase(Cases cases) {
        String chId = UUID.randomUUID().toString().replaceAll("-", "");
        System.out.println(cases.getChId());
        cases.setChId(chId);

        return casesMapper.insertCase(cases);
    }

    @Override
    public Cases selectByregId(String regId) {
        return   casesMapper.selectByregId(regId);

    }
}
