package com.ruoyi.project.lys.service;

import com.ruoyi.project.lys.domain.Cases;

/**
 * @author: author
 * @date: 20:42 2021/7/15
 */
public interface CasesService {
    int insertCase(Cases cases);

    Cases selectByregId(String regId);
}
