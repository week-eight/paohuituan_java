package com.ruoyi.project.lys.mapper;

import com.ruoyi.project.lys.domain.Patient;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: author
 * @date: 14:04 2021/7/15
 */
@Mapper
public interface PatientMapper  {
    //根据流水号查患者信息
    Patient selectOne(int id);
}
