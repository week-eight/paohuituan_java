package com.ruoyi.project.lys.mapper;

import com.ruoyi.project.lys.domain.Patient;
import com.ruoyi.project.lys.domain.Registration;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: author
 * @date: 21:14 2021/7/13
 */
@Mapper
public interface RegistrationMapper {
    //根据患者就诊状态查询患者（医生就诊）
     List<Registration> selectByState(String state);

     //根据流水编号修改患者挂号状态
    int changeState(int id);
}
