package com.ruoyi.project.lys.mapper;

import com.ruoyi.project.lys.domain.Cases;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: author
 * @date: 20:09 2021/7/15
 */
@Mapper
public interface CasesMapper {
    //添加病例
    int insertCase(Cases cases);

    //按挂号单id查询病例
    Cases selectByregId(String regId);
}
