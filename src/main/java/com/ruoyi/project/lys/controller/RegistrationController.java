package com.ruoyi.project.lys.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.lys.domain.Cases;
import com.ruoyi.project.lys.domain.Patient;
import com.ruoyi.project.lys.domain.Registration;
import com.ruoyi.project.lys.mapper.PatientMapper;
import com.ruoyi.project.lys.service.CasesService;
import com.ruoyi.project.lys.service.PatientService;
import com.ruoyi.project.lys.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: author
 * @date: 20:04 2021/7/13
 */
@RestController
@RequestMapping("/lys/treatment")
public class RegistrationController extends BaseController {
    @Autowired
    public RegistrationService registrationService;

    @Autowired
    private PatientService patientService;

    @Autowired
    private CasesService casesService;

    @PreAuthorize("@ss.hasPermi('lys:treatment:select')")
    @RequestMapping("/queryToBeSeenRegistration")
    public TableDataInfo RegistrationInfo(String state){

        List<Registration> list = registrationService.find(state);
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('lys:treatment:find')")
    @RequestMapping("/receivePatient/{id}")
    public boolean change(@PathVariable int id){
        int i = registrationService.changeState(id);
        if(i>0){
            return true;
        }else {
            return false;
        }
    }


    @PreAuthorize("@ss.hasPermi('lys:treatment:find')")
    @RequestMapping("/getPatientAllMessageByPatientId/{id}")
    public AjaxResult PatientInfo(@PathVariable int id){
        registrationService.changeState(id);
        Patient patient =patientService.findOne(id);
        return AjaxResult.success(patient);
    }

    @PreAuthorize("@ss.hasPermi('lys:treatment:case')")
    @PutMapping("/saveCareHistory")
    public AjaxResult addCases(@RequestBody  Cases cases){
        int rows = casesService.insertCase(cases);
        Cases cases1 = casesService.selectByregId(cases.getRegId());
        return AjaxResult.success(cases1);
    }

}
