package com.ruoyi.project.wuxuchen.hosregister.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorRes extends BaseEntity {
    private Integer dId;
    private String dName;
    private String dPhone;
    private Integer dAge;
    private String dSex;
    private String dLevel;
}
