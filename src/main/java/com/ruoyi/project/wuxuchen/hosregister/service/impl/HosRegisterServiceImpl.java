package com.ruoyi.project.wuxuchen.hosregister.service.impl;

import com.ruoyi.project.wuxuchen.hosregister.domain.DoctorRes;
import com.ruoyi.project.wuxuchen.hosregister.domain.Patient12;
import com.ruoyi.project.wuxuchen.hosregister.mapper.HosRegisterMapper;
import com.ruoyi.project.wuxuchen.hosregister.service.HosRegisterService;
import com.ruoyi.project.wuxuchen.registerlist.domian.RegisterList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class HosRegisterServiceImpl implements HosRegisterService {
    @Autowired
    private HosRegisterMapper hosRegisterMapper;

    private int i=30;


    @Override
    public Patient12 findOneByID(String id) {
        return hosRegisterMapper.selOneByID(id);
    }

    @Override
    public List<DoctorRes> selDoctors(String deptName) {
        return hosRegisterMapper.selDeptDoc(deptName);
    }

    @Override
    public Integer tianjiaAll(Patient12 patient12, RegisterList registerList, String did) {
        Patient12 p = hosRegisterMapper.selOneByID(patient12.getPatientIdentity());
        if(p==null){
            patient12.setPatientState("1");
            patient12.setPatientStart(new Date());
            patient12.setPatientAllergy("无");
            hosRegisterMapper.insertPatient(patient12);
            registerList.setRegistrationName(patient12.getPatientName());
        }
        registerList.setRegistrationPatientid(i++);
        registerList.setRegistrationName(hosRegisterMapper.selPatientName(patient12.getPatientIdentity()));

        registerList.setRegistrationMoney(hosRegisterMapper.selPrice(registerList.getRegistrationType()));

        registerList.setRegistrationDoctor(hosRegisterMapper.selDoctorName(did));
        registerList.setRegistrationState("待就诊");

        registerList.setRegistrationDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        int a= Integer.parseInt(new SimpleDateFormat("HH").format(new Date()));
        System.out.println(a);
        if(a>5&&a<12){
            registerList.setRegistrationTime("上午");
        }else if(a<19&& a>11){
            registerList.setRegistrationTime("下午");
        }else{
            registerList.setRegistrationTime("晚上");
        }
        System.out.println(registerList);
        Integer integer = hosRegisterMapper.insertRegisterList(registerList);
        System.out.println("4");
        System.out.println(integer);


        return integer;
    }
}
