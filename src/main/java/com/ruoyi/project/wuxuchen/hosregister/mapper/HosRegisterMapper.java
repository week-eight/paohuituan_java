package com.ruoyi.project.wuxuchen.hosregister.mapper;

import com.ruoyi.project.wuxuchen.hosregister.domain.DoctorRes;
import com.ruoyi.project.wuxuchen.hosregister.domain.Patient12;
import com.ruoyi.project.wuxuchen.registerlist.domian.RegisterList;

import java.util.List;

public interface HosRegisterMapper {

    Patient12 selOneByID(String id);

    List<DoctorRes> selDeptDoc(String deptName);

    Patient12 selPatientByID(int id);

    Integer insertPatient(Patient12 patient12);

    Integer insertRegisterList(RegisterList registerList);

    Double selPrice(String registrationType);

    String selDoctorName(String did);

    String selPatientName(String id);
}
