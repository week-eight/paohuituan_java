package com.ruoyi.project.wuxuchen.hosregister.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Patient12 extends BaseEntity {
    //`patient_id` int NOT NULL AUTO_INCREMENT COMMENT '流水号',
    //  `patient_name` varchar(20) NOT NULL COMMENT '患者姓名',
    //  `patient_phone` varchar(20) NOT NULL COMMENT '患者电话',
    //  `patient_identity` varchar(20) NOT NULL COMMENT '患者身份证号',
    //  `patient_birth` datetime NOT NULL COMMENT '患者出生日期',
    //  `patient_age` int NOT NULL COMMENT '患者年龄',
    //  `patient_sex` varchar(2) NOT NULL COMMENT '患者性别',
    //  `patient_state` varchar(20) NOT NULL COMMENT '患者信息状态',
    //  `patient_start` datetime NOT NULL COMMENT '患者创建时间',
    //  `patient_address` varchar(255) NOT NULL COMMENT '患者地址信息',
    //  `patient_allergy` varchar(255) NOT NULL COMMENT '患者过敏史',
    //  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
    //  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    //  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
    //  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    //  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
    //  PRIMARY KEY (`patient_id`)
    //
    private Integer patientId;
    private String patientName;
    private String patientPhone;
    private String patientIdentity;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private String patientBirth;

    private Integer patientAge;
    private String patientSex;
    private String patientState;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date patientStart;
    private String patientAddress;
    private String patientAllergy;

    public Patient12(String patientIdentity) {
        this.patientIdentity = patientIdentity;
    }
}
