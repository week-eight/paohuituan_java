package com.ruoyi.project.wuxuchen.hosregister.service;

import com.ruoyi.project.wuxuchen.hosregister.domain.DoctorRes;
import com.ruoyi.project.wuxuchen.hosregister.domain.Patient12;
import com.ruoyi.project.wuxuchen.registerlist.domian.RegisterList;

import java.util.List;

public interface HosRegisterService {
    Patient12 findOneByID(String id);

    List<DoctorRes> selDoctors(String deptName);

    Integer tianjiaAll(Patient12 patient12, RegisterList registerList, String did);
}
