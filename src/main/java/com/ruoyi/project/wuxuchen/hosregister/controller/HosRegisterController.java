package com.ruoyi.project.wuxuchen.hosregister.controller;

import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.wuxuchen.hosregister.domain.Patient12;
import com.ruoyi.project.wuxuchen.hosregister.service.HosRegisterService;
import com.ruoyi.project.wuxuchen.registerlist.domian.RegisterList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/wuxuchen/hosregister")
public class HosRegisterController {
    String did;

    @Autowired
    private HosRegisterService hosRegisterService;

    @RequestMapping("/findPatientID")
    public AjaxResult findPatiengID(String patientIdentity){
        Patient12 oneByID = hosRegisterService.findOneByID(patientIdentity);
        return AjaxResult.success(oneByID);

    }

    @GetMapping("/findDeptHos")
    public AjaxResult listDeptHos(String deptName1) {
        return AjaxResult.success(hosRegisterService.selDoctors(deptName1));
    }
    @GetMapping("/tijiao")
    public AjaxResult tijiaoAll(Patient12 patient12, RegisterList registerList) {
        Integer integer = hosRegisterService.tianjiaAll(patient12, registerList, did);
        return AjaxResult.success(integer);
    }

    @GetMapping("/tijiao1")
    public AjaxResult tijiaoAll1(String ddd) {
        did=ddd;
        return null;
    }

}
