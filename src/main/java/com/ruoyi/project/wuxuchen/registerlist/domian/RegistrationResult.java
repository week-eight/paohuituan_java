package com.ruoyi.project.wuxuchen.registerlist.domian;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationResult extends BaseEntity {
    private String registrationName;
    private Double registrationPrice;

}
