package com.ruoyi.project.wuxuchen.registerlist.service;

import com.ruoyi.project.wuxuchen.registerlist.domian.DeptName;
import com.ruoyi.project.wuxuchen.registerlist.domian.RegisterList;
import com.ruoyi.project.wuxuchen.registerlist.domian.RegistrationResult;

import java.util.List;

public interface IRegisterListService {
    List<RegisterList> selectList(RegisterList registerList);

    List<DeptName> selectAllDeptName();

    List<RegistrationResult> selAll();
}
