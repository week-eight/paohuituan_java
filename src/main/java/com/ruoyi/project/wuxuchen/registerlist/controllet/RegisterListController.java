package com.ruoyi.project.wuxuchen.registerlist.controllet;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.wuxuchen.registerlist.domian.RegisterList;
import com.ruoyi.project.wuxuchen.registerlist.service.IRegisterListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/wuxuchen/registerlist")
public class RegisterListController extends BaseController {
    @Autowired
    private IRegisterListService registerListService;


    @GetMapping("/list")
    @PreAuthorize("@ss.hasPermi('wuxuchen:register:list')")
    public TableDataInfo list(RegisterList registerList){
        //分页相关
        startPage();
        //调用service 查询数据库
        List<RegisterList> list= registerListService.selectList(registerList);
        return getDataTable(list);
    }
    @GetMapping("/all")
    public AjaxResult findAllDeptName(){
        return AjaxResult.success(registerListService.selectAllDeptName());
    }
    @GetMapping("/allr")
    public AjaxResult findAllRegisType(){
        return AjaxResult.success(registerListService.selAll());
    }


}
