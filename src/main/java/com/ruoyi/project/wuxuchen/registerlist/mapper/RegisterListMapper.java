package com.ruoyi.project.wuxuchen.registerlist.mapper;

import com.ruoyi.project.wuxuchen.registerlist.domian.DeptName;
import com.ruoyi.project.wuxuchen.hosregister.domain.DoctorRes;
import com.ruoyi.project.wuxuchen.registerlist.domian.RegisterList;
import com.ruoyi.project.wuxuchen.registerlist.domian.RegistrationResult;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface RegisterListMapper {

    List<RegistrationResult> selectAllregisterType();

    List<DeptName> selectAllDeptName();

    List<RegisterList> selectList(RegisterList registerList);


}
