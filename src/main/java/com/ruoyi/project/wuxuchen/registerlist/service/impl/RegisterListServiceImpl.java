package com.ruoyi.project.wuxuchen.registerlist.service.impl;

import com.ruoyi.project.wuxuchen.registerlist.domian.DeptName;
import com.ruoyi.project.wuxuchen.registerlist.domian.RegisterList;
import com.ruoyi.project.wuxuchen.registerlist.domian.RegistrationResult;
import com.ruoyi.project.wuxuchen.registerlist.mapper.RegisterListMapper;
import com.ruoyi.project.wuxuchen.registerlist.service.IRegisterListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RegisterListServiceImpl implements IRegisterListService {
    @Autowired
    private RegisterListMapper registerListMapper;
    @Override
    public List<RegisterList> selectList(RegisterList registerList) {

        return registerListMapper.selectList(registerList);
    }

    @Override
    public List<DeptName> selectAllDeptName() {
        System.out.println("1");
        System.out.println(registerListMapper.selectAllDeptName());
        return registerListMapper.selectAllDeptName();
    }

    @Override
    public List<RegistrationResult> selAll() {
        return registerListMapper.selectAllregisterType();
    }
}
