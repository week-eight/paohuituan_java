package com.ruoyi.project.wuxuchen.registerlist.domian;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterList extends BaseEntity {
    //on_id` varchar(30) NOT NULL COMMENT '挂号单ID',
    // `registration_patientid` int DEFAULT NULL COMMENT '流水号',

    //  `registration_name` varchar(10) NOT NULL COMMENT '患者姓名',
    //  `registration_depart` varchar(10) NOT NULL COMMENT '挂号科室',
    //  `registration_doctor` varchar(10) NOT NULL COMMENT '接诊医生',
    //  `registration_money` double(10,0) NOT NULL COMMENT '挂号费用',
    //  `registration_state` varchar(10) NOT NULL COMMENT '挂号状态',
    //  `registration_date` datetime NOT NULL COMMENT '就诊日期',
    //  `registration_type` varchar(10) NOT NULL COMMENT '挂号类型',
    //  `registration_time` varchar(10) NOT NULL COMMENT '挂号时段（下午）',
    //

    private String registrationId;
    private Integer registrationPatientid;
    private String registrationName;
    private String registrationDepart;
    private String registrationDoctor;
    private Double registrationMoney;
    private String registrationState;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String registrationDate;
    private String registrationType;
    private String registrationTime;


}