package com.ruoyi.project.weekeight.controller;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.weekeight.domain.SystemRegistration;
import com.ruoyi.project.weekeight.service.IRegistrationService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 挂号费用设置控制器
 * </p>
 * <b>ClassName:</b><br>
 * &emsp;&emsp;&emsp;RegistrationController
 * <br><b>Description:</b><br>
 * &emsp;&emsp;&emsp;挂号费用设置控制器
 * <br><b>Date:</b><br>
 * &emsp;&emsp;&emsp;2021/7/13 15:49
 *
 * @author: week_eight
 * @version: v1.0
 */
@RestController
@RequestMapping("/system/registration")
public class RegistrationCostController extends BaseController {
    @Autowired
    private IRegistrationService iRegistrationService;
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;挂号费用查询控制
     * @param systemRegistration 挂号费用实体类
     * @return com.ruoyi.framework.web.page.TableDataInfo-分页返回
     */
    @PreAuthorize("@ss.hasPermi('system:registration:select')")
    @GetMapping("/list")
    public TableDataInfo list(SystemRegistration systemRegistration) {
        //分页相关,处理请求参数pageNum,pageSize等，并使用pageHelper处理分页查询。
        startPage();
        //调用service查询数据库
        List<SystemRegistration> list = iRegistrationService.findList(systemRegistration);
        //返回
        return getDataTable(list);
    }
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;挂号费用添加控制
     * @param systemRegistration 挂号费用实体类
     * @return com.ruoyi.framework.web.domain.AjaxResult
     */
    @PostMapping("/add")
    @PreAuthorize("@ss.hasPermi('system:registration:add')")
    public AjaxResult addRegistration(@RequestBody SystemRegistration systemRegistration) {
        //设置谁创建的-创建人
        systemRegistration.setCreateBy(SecurityUtils.getUsername());
        int rows = iRegistrationService.addRegistration(systemRegistration);
        return toAjax(rows);
    }
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;修改主键查询回显
     * @param registrationId 主键
     * @return com.ruoyi.framework.web.domain.AjaxResult-不分页
     */
    @GetMapping("/{registrationId}")
    @PreAuthorize("@ss.hasPermi('system:registration:select')")
    public AjaxResult getById(@PathVariable Long registrationId) {
        SystemRegistration systemRegistration = iRegistrationService.getById(registrationId);
        return AjaxResult.success(systemRegistration);
    }
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;挂号费用修改控制
     * @param systemRegistration 挂号费用实体类
     * @return com.ruoyi.framework.web.domain.AjaxResult
     */
    @PutMapping
    @PreAuthorize("@ss.hasPermi('system:registration:edit')")
    public AjaxResult editRegistration(@RequestBody SystemRegistration systemRegistration) {
        //设置谁修改的-修改人
        systemRegistration.setCreateBy(SecurityUtils.getUsername());
        return toAjax(iRegistrationService.editRegistration(systemRegistration));
    }
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;挂号费用删除控制
     * @param registrationIds 主键(多)
     * @return com.ruoyi.framework.web.domain.AjaxResult
     */
    @DeleteMapping("/{registrationIds}")
    @PreAuthorize("@ss.hasPermi('system:user:remove')")
    public AjaxResult removeRegistration(@PathVariable Long[] registrationIds ) {
        return toAjax(iRegistrationService.registrationIds(registrationIds));
    }
}
