package com.ruoyi.project.weekeight.mapper;

import com.ruoyi.project.weekeight.domain.SystemRegistration;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *
 * </p>
 * <b>ClassName:</b><br>
 * &emsp;&emsp;&emsp;RegistrationMapper
 * <br><b>Description:</b><br>
 * &emsp;&emsp;&emsp;处理挂号费用数据接口
 * <br><b>Date:</b><br>
 * &emsp;&emsp;&emsp;2021/7/13 16:09
 *
 * @author: week_eight
 * @version: v1.0
 */
@Mapper
public interface RegistrationCostMapper {
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;根据条件，查询挂号费用
     * @param systemRegistration 挂号费用实体类
     * @return java.util.List<com.ruoyi.project.weekeight.domain.SystemRegistration>
     */
    List<SystemRegistration> selectList(SystemRegistration systemRegistration);
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;新增挂号费用
     * @param systemRegistration 挂号费实体类
     * @return int 返回结果
     */
    int insert(SystemRegistration systemRegistration);
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;主键查询
     * @param registrationId 主键
     * @return com.ruoyi.project.weekeight.domain.SystemRegistration
     */
    SystemRegistration selectById(Long registrationId);
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;修改挂号费用
     * @param systemRegistration 挂号费实体类
     * @return int 返回结果
     */
    int update(SystemRegistration systemRegistration);
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;挂号费用删除控制
     * @param registrationIds 主键(多)
     * @return com.ruoyi.framework.web.domain.AjaxResult
     */
    int batchDelete(@Param("registrationIds") Long[] registrationIds);
}
