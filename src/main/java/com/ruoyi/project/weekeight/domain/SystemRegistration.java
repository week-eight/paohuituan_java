package com.ruoyi.project.weekeight.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 *
 * </p>
 * <b>ClassName:</b><br>
 * &emsp;&emsp;&emsp;SystemRegistration
 * <br><b>Description:</b><br>
 * &emsp;&emsp;&emsp;挂号实体类
 * <br><b>Date:</b><br>
 * &emsp;&emsp;&emsp;2021/7/13 13:03
 *
 * @author: week_eight
 * @version: v1.0
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SystemRegistration extends BaseEntity {
    //挂号费ID
    private long registrationId;
    //挂号名称
    private String registrationName;
    //挂号费
    private double registrationPrice;
    //状态（0正常 1停用）
    private String registrationStatus;
}
