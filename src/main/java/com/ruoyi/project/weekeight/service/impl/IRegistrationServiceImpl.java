package com.ruoyi.project.weekeight.service.impl;

import com.ruoyi.project.weekeight.domain.SystemRegistration;
import com.ruoyi.project.weekeight.mapper.RegistrationCostMapper;
import com.ruoyi.project.weekeight.service.IRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * </p>
 * <b>ClassName:</b><br>
 * &emsp;&emsp;&emsp;IRegistrationServiceImpl
 * <br><b>Description:</b><br>
 * &emsp;&emsp;&emsp;实现挂号费用业务
 * <br><b>Date:</b><br>
 * &emsp;&emsp;&emsp;2021/7/13 16:04
 *
 * @author: week_eight
 * @version: v1.0
 */
@Service
public class IRegistrationServiceImpl implements IRegistrationService {
    @Autowired
    private RegistrationCostMapper registrationMapper;
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;查询挂号费用
     * @param systemRegistration 查询条件
     * @return java.util.List<com.ruoyi.project.weekeight.domain.SystemRegistration>
     */
    @Override
    public List<SystemRegistration> findList(SystemRegistration systemRegistration) {
        return registrationMapper.selectList(systemRegistration);
    }
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;新增挂号费用
     * @param systemRegistration 挂号费实体类
     * @return int 返回结果
     */
    @Override
    public int addRegistration(SystemRegistration systemRegistration) {
        //考虑是否有数据需要赋值-创建时间
        systemRegistration.setCreateTime(new Date());
        return registrationMapper.insert(systemRegistration);
    }
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;修改主键查询回显
     * @param registrationId 主键
     * @return com.ruoyi.project.weekeight.domain.SystemRegistration
     */
    @Override
    public SystemRegistration getById(Long registrationId) {
        return registrationMapper.selectById(registrationId);
    }
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;修改挂号费用
     * @param systemRegistration 挂号费实体类
     * @return int 返回结果
     */
    @Override
    public int editRegistration(SystemRegistration systemRegistration) {
        //考虑是否有数据需要赋值-修改时间
        systemRegistration.setCreateTime(new Date());
        return registrationMapper.update(systemRegistration);
    }
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;挂号费用删除控制
     * @param registrationIds 主键(多)
     * @return com.ruoyi.framework.web.domain.AjaxResult
     */
    @Override
    public int registrationIds(Long[] registrationIds) {
        return registrationMapper.batchDelete(registrationIds);
    }
}
