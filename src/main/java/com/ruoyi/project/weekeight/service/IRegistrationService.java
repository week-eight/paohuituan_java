package com.ruoyi.project.weekeight.service;

import com.ruoyi.project.weekeight.domain.SystemRegistration;

import java.util.List;

/**
 * <p>
 *
 * </p>
 * <b>ClassName:</b><br>
 * &emsp;&emsp;&emsp;IRegistrationService
 * <br><b>Description:</b><br>
 * &emsp;&emsp;&emsp;业务接口
 * <br><b>Date:</b><br>
 * &emsp;&emsp;&emsp;2021/7/13 16:04
 *
 * @author: week_eight
 * @version: v1.0
 */
public interface IRegistrationService {
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;根据查询条件，访问数据库，查询挂号费用
     * 分页逻辑使用PageHelper实现
     * @param systemRegistration 挂号费实体类
     * @return java.util.List<com.ruoyi.project.weekeight.domain.SystemRegistration>
     */
    List<SystemRegistration> findList(SystemRegistration systemRegistration);
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;新增挂号费用
     * @param systemRegistration 挂号费实体类
     * @return int 返回结果
     */
    int addRegistration(SystemRegistration systemRegistration);
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;修改主键查询回显
     * @param registrationId 主键
     * @return com.ruoyi.project.weekeight.domain.SystemRegistration
     */
    SystemRegistration getById(Long registrationId);
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;修改挂号费用
     * @param systemRegistration 挂号费实体类
     * @return int 返回结果
     */
    int editRegistration(SystemRegistration systemRegistration);
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;挂号费用删除控制
     * @param registrationIds 主键(多)
     * @return com.ruoyi.framework.web.domain.AjaxResult
     */
    int registrationIds(Long[] registrationIds);
}
