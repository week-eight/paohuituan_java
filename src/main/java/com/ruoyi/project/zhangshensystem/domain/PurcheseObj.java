package com.ruoyi.project.zhangshensystem.domain;


import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode
public class PurcheseObj extends BaseEntity {
        private PurchaseDto purchaseDto;

        private PurchaseItemDtos[] purchaseItemDtos;


}
