package com.ruoyi.project.zhangshensystem.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode
public class PurchaseItemDtos {
    private Integer infoId;
    private String infoName;
    private String infoCode; //药品编码
    private String infoPrice; //处方类型
    private String infoUnit; //单位
    private String keywords; //关键字
    private String productionName;//药品名称
    private String status ; //状态
    private Integer purchaseNumber; //药品数量
    private Float tradePrice ; //批发价
    private Float tradeTotalAmount; //批发额
    private Integer batchNumber ; //批次号
    private String  remark; //备注

}
