package com.ruoyi.project.zhangshensystem.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode
public class PurchaseDto {
        private String purchaseId;
        private String providerOrderName;
        private Integer purchaseTradeTotalAmount;
}
