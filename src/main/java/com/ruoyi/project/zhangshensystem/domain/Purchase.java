package com.ruoyi.project.zhangshensystem.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode
public class Purchase extends BaseEntity {
    private String purchase_id;
    private String purchase_name;
    private Integer purchase_count;
    private String purchase_status;
    private String purchase_people;
    private String purchase_onpeople;
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:MM:ss" )
    @JsonFormat(pattern = "yyyy-MM-dd hh:MM:ss")
    private Date purchase_ontime;
    private String create_by;
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:MM:ss" )
    @JsonFormat(pattern = "yyyy-MM-dd hh:MM:ss")
    private Date create_time;
    private String status;
}
