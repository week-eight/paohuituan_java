package com.ruoyi.project.zhangshensystem.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode
public class Supplier extends BaseEntity {
    private Integer supplier_id;
    private String supplier_name;
    private String supplier_people;
    private String supplier_phone;
    private String supplier_blank;
    private String supplier_area;
    private String supplier_status;
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:MM:ss" )
    @JsonFormat(pattern = "yyyy-MM-dd hh:MM:ss")
    private Date supplier_time;


}
