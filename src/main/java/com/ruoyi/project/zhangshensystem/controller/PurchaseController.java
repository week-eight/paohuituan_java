package com.ruoyi.project.zhangshensystem.controller;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;

import com.ruoyi.project.zhangshensystem.domain.Purchase;


import com.ruoyi.project.zhangshensystem.domain.PurchaseDto;
import com.ruoyi.project.zhangshensystem.domain.PurchaseItemDtos;
import com.ruoyi.project.zhangshensystem.domain.PurcheseObj;
import com.ruoyi.project.zhangshensystem.service.IPurchaseService;
import com.ruoyi.project.zmr.domain.Info;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/system/purchase")
public class PurchaseController extends BaseController {
    @Autowired
    private IPurchaseService iPurchaseService;



    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:purchase:query')")
    @GetMapping("/list")
    public TableDataInfo list(Purchase purchase){
        startPage();
        List<Purchase> purchaseList = iPurchaseService.list(purchase);
    return getDataTable(purchaseList);
        }
//查询订单审核
    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:purchase:query')")
    @GetMapping("/lists")
    public TableDataInfo lists(Purchase purchase){
        startPage();
        List<Purchase> purchaseLists = iPurchaseService.lists(purchase);
        return getDataTable(purchaseLists);
    }

    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:purchase:query')")
    @GetMapping("/selectAllPurchase")
    public AjaxResult selectAllSupplier(){
        List<Purchase> purchase = iPurchaseService.selectAllPurchaseService();
        System.out.println("purchase:"+purchase);
        return AjaxResult.success(purchase);
    }
    //订单编号
    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:supplier:index')")
    @GetMapping("/danju")
    public AjaxResult danju(){
        Integer i =(int)(Math.random()*10000)+1 ;
        System.out.println(i);
        String a = "CG202107"+i;
        return AjaxResult.success(a);
    }
    //查询药品
    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:supplier:index')")
    @GetMapping("/listMedicinesForPage")
    public TableDataInfo listMedicinesForPage(){
        startPage();
        List<Info> info =  iPurchaseService.listMedicnesForPage();
        System.out.println(info);
        return getDataTable(info);
    }

    //添加采购
    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:supplier:index')")
    @PostMapping
    public AjaxResult addPurchaseToAudit(@RequestBody PurcheseObj purcheseObj ){
        System.out.println(purcheseObj);
        purcheseObj.setCreateBy(SecurityUtils.getUsername());

        int p = iPurchaseService.addPurchase(purcheseObj);


        return toAjax(1);
    }

    //查询药品信息
    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:supplier:index')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getById(@PathVariable String userId){
        PurchaseItemDtos purchaseItemDtos =  iPurchaseService.getById(userId);
        return AjaxResult.success(purchaseItemDtos);
    }


}
