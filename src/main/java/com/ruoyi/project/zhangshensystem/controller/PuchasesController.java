package com.ruoyi.project.zhangshensystem.controller;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.zhangshensystem.domain.Purchase;
import com.ruoyi.project.zhangshensystem.domain.PurchaseItemDtos;
import com.ruoyi.project.zhangshensystem.service.IPurchaseService;
import org.apache.ibatis.annotations.Delete;
import org.apache.poi.xssf.usermodel.XSSFPivotTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/system/purchases")
public class PuchasesController extends BaseController {

    @Autowired
    private IPurchaseService iPurchaseService;

    //查询药品信息
    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:supplier:index')")
    @GetMapping(value = "/{purchase_id}")
    public AjaxResult doAudit(@PathVariable String purchase_id , Purchase purchase){
        System.out.println(purchase.getPurchase_id());
        purchase.setCreateBy(SecurityUtils.getUsername());
        purchase.setCreateTime(new Date());
        purchase.setStatus("1");
        int a = iPurchaseService.addDingdan(purchase);
        return AjaxResult.success(1);
    }


    //查询药品信息
    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:supplier:index')")
    @PutMapping("/{purchaseId}")
    public AjaxResult succesPass(@PathVariable  String purchaseId ){
        System.out.println(purchaseId);
        int rows = iPurchaseService.succesPass(purchaseId);
        return toAjax(rows);
    }

    //查询药品信息
    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:supplier:index')")
    @DeleteMapping("/{lowId}")
    public AjaxResult lowPass(@PathVariable  String lowId ){

        int rows = iPurchaseService.lowPass(lowId);
        return toAjax(rows);
    }


}
