package com.ruoyi.project.zhangshensystem.controller;


import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.zhangshensystem.domain.Supplier;
import com.ruoyi.project.zhangshensystem.service.ISupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/system/supplier")
public class SupplierController extends BaseController {

    @Autowired
    private ISupplierService iSupplierService;


    @DeleteMapping("/{deptId}")
    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:supplier:delete')")
    public AjaxResult delSupper(@PathVariable Long[] deptId){
        int rows = iSupplierService.delSupper(deptId);
        return toAjax(rows);
    }



    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:supplier:update')")
    @PutMapping
    public AjaxResult updateSupper(@RequestBody Supplier supplier){
        System.out.println("supplier:"+supplier);
        supplier.setUpdateBy(SecurityUtils.getUsername());
        int row = iSupplierService.change(supplier);
        return toAjax(row);
    }


    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:supplier:index')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getById(@PathVariable Long userId){
       Supplier supplier =  iSupplierService.getById(userId);
        return AjaxResult.success(supplier);
    }


    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:supplier:index')")
    @GetMapping("/selectAllSupplier")
    public AjaxResult selectAllSupplier(){
       List<Supplier> supplierList =  iSupplierService.selectAllSupplier();
        return AjaxResult.success(supplierList);
    }





    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:supplier:index')")
    @GetMapping("/list")
    public TableDataInfo list(Supplier supplier){
        System.out.println(supplier.getSupplier_name());
        startPage();
       List<Supplier> list =  iSupplierService.select(supplier);
        System.out.println(list);
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasAnyPermi('zhangshensystem:supplier:add')")
    @PostMapping
    public AjaxResult addSupplier(@RequestBody Supplier supplier){
        supplier.setCreateBy(SecurityUtils.getUsername());
        System.out.println(supplier);
        int rows =  iSupplierService.addSupplist(supplier);
        return toAjax(rows);
    }



}
