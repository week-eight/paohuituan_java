package com.ruoyi.project.zhangshensystem.mapper;

import com.ruoyi.project.zhangshensystem.domain.Purchase;
import com.ruoyi.project.zhangshensystem.domain.PurchaseItemDtos;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PurchaseItemDtosMapper {
    PurchaseItemDtos selectItem(String userId);


}
