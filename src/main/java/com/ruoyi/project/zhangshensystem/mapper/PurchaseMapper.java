package com.ruoyi.project.zhangshensystem.mapper;


import com.ruoyi.project.zhangshensystem.domain.Purchase;
import com.ruoyi.project.zmr.domain.Info;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

@Mapper
public interface PurchaseMapper {
    List<Purchase> selectAll(Purchase purchase);

    List<Purchase> selectname();

    //查询药品
    List<Info> selectInfo();
        //订单信息

    int addPurchase(String purchaseId, String purchaseName, Integer purchaseCount, String createBy, Date createTime);

    int addItem(String purchaseId,
                String infoName,
                String infoCode,
                String infoPrice,
                String infoUnit,
                String keywords,
                String productionName,
                Integer purchaseNumber,
                Float tradePrice,
                Float tradeTotalAmount,
                Integer batchNumber,
                String  remark
    );

    List<Purchase> selectList(Purchase purchase);

    int updateDd(String purchase_id, String createBy, Date createTime, String status);


    int successUpdate(String purchaseId);

    int lowUpdate(String lowId);
}
