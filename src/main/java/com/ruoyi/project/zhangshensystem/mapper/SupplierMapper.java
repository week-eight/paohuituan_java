package com.ruoyi.project.zhangshensystem.mapper;

import com.ruoyi.project.zhangshensystem.domain.Supplier;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SupplierMapper {


    List<Supplier> selectList(Supplier supplier);

    int insert(Supplier supplier);

    Supplier selectOne(Long deptId);

    int update(Supplier supplier);

    int delete(@Param("deptIds") Long[] deptId);

    List<Supplier> selectAll();


}
