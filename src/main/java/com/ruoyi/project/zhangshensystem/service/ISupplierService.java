package com.ruoyi.project.zhangshensystem.service;

import com.ruoyi.project.zhangshensystem.domain.Supplier;

import java.util.List;

public interface ISupplierService {

  //查询供应商
    List<Supplier> select(Supplier supplier);

    int addSupplist(Supplier supplier);
    //修改前查询
     Supplier getById(Long deptId);

  int change(Supplier supplier);

  int delSupper(Long[] deptId);


  List<Supplier> selectAllSupplier();
}
