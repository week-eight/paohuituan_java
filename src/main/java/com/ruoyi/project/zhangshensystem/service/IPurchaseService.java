package com.ruoyi.project.zhangshensystem.service;


import com.ruoyi.project.zhangshensystem.domain.Purchase;

import com.ruoyi.project.zhangshensystem.domain.PurchaseItemDtos;
import com.ruoyi.project.zhangshensystem.domain.PurcheseObj;
import com.ruoyi.project.zmr.domain.Info;

import java.util.List;

public interface IPurchaseService {


    List<Purchase> list(Purchase purchase);

    List<Purchase> selectAllPurchaseService();

    List<Info> listMedicnesForPage();
    //添加订单信息
    int addPurchase(PurcheseObj purcheseObj );
   //查询药品信息
    PurchaseItemDtos getById(String userId);
    //查询订单信息
    List<Purchase> lists(Purchase purchase);

    int addDingdan(Purchase purchase);

    //通过订单
    int succesPass(String purchaseId);

    //删除订单
    int lowPass(String lowId);
}
