package com.ruoyi.project.zhangshensystem.service.impl;


import com.ruoyi.project.zhangshensystem.domain.Purchase;
import com.ruoyi.project.zhangshensystem.domain.PurchaseItemDtos;
import com.ruoyi.project.zhangshensystem.domain.PurcheseObj;
import com.ruoyi.project.zhangshensystem.mapper.PurchaseItemDtosMapper;
import com.ruoyi.project.zhangshensystem.mapper.PurchaseMapper;
import com.ruoyi.project.zhangshensystem.service.IPurchaseService;
import com.ruoyi.project.zmr.domain.Info;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@Service
public class PurchaseServiceImpl implements IPurchaseService {
    @Autowired
    private PurchaseMapper  purchaseMapper;

    @Autowired
    private PurchaseItemDtosMapper purchaseItemDtosMapper;

    @Override
    public List<Purchase> list(Purchase purchase) {
        return purchaseMapper.selectAll(purchase);
    }

    @Override
    public List<Purchase> selectAllPurchaseService() {
        return purchaseMapper.selectname();
    }

    @Override
    public List<Info> listMedicnesForPage() {
        return purchaseMapper.selectInfo();
    }

    @Override
    public int addPurchase(PurcheseObj purcheseObj) {
        String purchaseId = purcheseObj.getPurchaseDto().getPurchaseId();
        String purchaseName = purcheseObj.getPurchaseDto().getProviderOrderName();
        Integer purchaseCount = purcheseObj.getPurchaseDto().getPurchaseTradeTotalAmount();
        String createBy = purcheseObj.getCreateBy();
        purcheseObj.setCreateTime(new Date());
        Date createTime = purcheseObj.getCreateTime();
        int i = purchaseMapper.addPurchase(purchaseId, purchaseName, purchaseCount, createBy, createTime);

//         for (int a=0;a<purcheseObj.getPurchaseItemDtos().length;i++){
//
//         }

        int a = 0;
        for (PurchaseItemDtos b:purcheseObj.getPurchaseItemDtos()
             ) {

            a =  purchaseMapper.addItem(purchaseId,
                   b.getInfoName(),
                   b.getInfoCode(),
                   b.getInfoPrice(),
                   b.getInfoUnit(),
                   b.getKeywords(),
                   b.getProductionName(),
                   b.getPurchaseNumber(),
                   b.getTradePrice(),
                   b.getTradeTotalAmount(),
                   b.getBatchNumber(),
                   b.getRemark());

        }

        return 1;
    }

    @Override
    public PurchaseItemDtos getById(String userId) {
        return purchaseItemDtosMapper.selectItem(userId);
    }

    @Override
    public List<Purchase> lists(Purchase purchase) {
        return purchaseMapper.selectList(purchase);
    }

    @Override
    public int addDingdan(Purchase purchase) {

        String purchase_id = purchase.getPurchase_id();
        String createBy = purchase.getCreateBy();
        Date createTime = purchase.getCreateTime();
        String status = purchase.getStatus();
        return purchaseMapper.updateDd(purchase_id,createBy,createTime,status);
    }

    @Override
    public int succesPass(String purchaseId) {
        return purchaseMapper.successUpdate(purchaseId);
    }

    @Override
    public int lowPass(String lowId) {
        return purchaseMapper.lowUpdate(lowId);
    }


}
