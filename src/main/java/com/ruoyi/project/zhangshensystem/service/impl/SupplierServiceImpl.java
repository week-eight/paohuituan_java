package com.ruoyi.project.zhangshensystem.service.impl;

import com.ruoyi.project.zhangshensystem.mapper.SupplierMapper;
import com.ruoyi.project.zhangshensystem.domain.Supplier;
import com.ruoyi.project.zhangshensystem.service.ISupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@Service
public class SupplierServiceImpl implements ISupplierService {

    @Autowired
    private SupplierMapper supplierMapper;

        //查询供应商
    @Override
    public List<Supplier> select(Supplier supplier) {
          List<Supplier> list =   supplierMapper.selectList(supplier);
        return list;
    }

    @Override
    public int addSupplist(Supplier supplier) {
        supplier.setSupplier_time(new Date());
        return supplierMapper.insert(supplier);
    }

    @Override
    public Supplier getById(Long deptId) {
        return supplierMapper.selectOne(deptId);
    }

    @Override
    public int change(Supplier supplier) {
        supplier.setUpdateTime(new Date());
        return supplierMapper.update(supplier);
    }

    @Override
    public int delSupper(Long[] deptId) {
        return supplierMapper.delete(deptId);
    }

    @Override
    public List<Supplier> selectAllSupplier() {
        return supplierMapper.selectAll();
    }


}
