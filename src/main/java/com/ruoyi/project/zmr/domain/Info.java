package com.ruoyi.project.zmr.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import com.ruoyi.project.qs.domain.DrugProduction;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author：ZMR
 * @DATE 17:09  2021/7/13
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Info extends BaseEntity {
    private Integer infoId; //药品ID
    private String infoName; //药品名称
    private String infoCode; //药品编码
    private String infoType; //药品类型
    private String infoPtype; //处方类型
    private String infoUnit; //单位
    private String infoPrice; //处方价格
    private String infoInventory; //库存量
    private Integer infoWarning; //预警值
    private Integer infoConversion; //换算量
    private String keywords; //关键字
    private Integer productId;//厂家ID
    private String status; //状态
    private Integer delFlag;//逻辑删除
    private DrugProduction production;
    private String productionName;//药品名称
}
