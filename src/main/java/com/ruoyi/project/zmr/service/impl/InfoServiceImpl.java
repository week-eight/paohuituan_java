package com.ruoyi.project.zmr.service.impl;

import com.ruoyi.project.zmr.domain.Info;
import com.ruoyi.project.zmr.mapper.InfoMapper;
import com.ruoyi.project.zmr.service.IInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author：ZMR
 * @DATE 18:54  2021/7/13
 */
@Service
public class InfoServiceImpl implements IInfoService {

    @Autowired
    private InfoMapper infoMapper;

    //查询所有药品信息
    @Override
    public List<Info> findList(Info info) {

        return infoMapper.selectList(info);
    }

    //添加药品信息
    @Override
    public int add(Info info) {
        return infoMapper.insert(info);
    }

    //根据主键查询药品信息
    @Override
    public Info findInfoById(int infoId) {
        return infoMapper.selectInfoById(infoId);
    }

    @Override
    public int modify(Info info) {
        return infoMapper.update(info);
    }

    @Override
    public int remove(int[] infoIds) {
        return infoMapper.delete(infoIds);
    }

    @Override
    public int reRemove() {
        return infoMapper.reDelete();
    }

}
