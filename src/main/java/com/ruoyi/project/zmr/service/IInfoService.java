package com.ruoyi.project.zmr.service;

import com.ruoyi.project.zmr.domain.Info;

import java.util.List;

/**
 * @Author：ZMR
 * @DATE 18:52  2021/7/13
 */
public interface IInfoService {

    List<Info> findList(Info info);

    int add(Info info);

    Info findInfoById(int infoId);

    int modify(Info info);

    int remove(int[] infoIds);

    int reRemove();
}
