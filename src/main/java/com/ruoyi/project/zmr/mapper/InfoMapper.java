package com.ruoyi.project.zmr.mapper;

import com.ruoyi.project.zmr.domain.Info;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author：ZMR
 * @DATE 17:16  2021/7/13
 */
@Mapper
public interface InfoMapper {
    List<Info> selectList(Info info);

    int insert(Info info);

    Info selectInfoById(int infoId);

    int update(Info info);

    int delete(int[] infoIds);

    int reDelete();
}
