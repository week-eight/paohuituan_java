package com.ruoyi.project.zmr.controller;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.qs.domain.DrugProduction;
import com.ruoyi.project.qs.service.DrugService;
import com.ruoyi.project.zmr.domain.Info;
import com.ruoyi.project.zmr.service.IInfoService;
import org.apache.catalina.security.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * @Author：ZMR
 * @DATE 18:55  2021/7/13
 */
@RestController
@RequestMapping("/drug/info")
public class InfoController extends BaseController {

    @Autowired
    private IInfoService iInfoService;

    @Autowired
    private DrugService drugService;



    @PreAuthorize("@ss.hasPermi('zmr:info:query')")
    @GetMapping("/list")
    public TableDataInfo list(Info info){
        startPage();
        List<Info> list = iInfoService.findList(info);
        list.forEach(System.out::println);
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('zmr:info:add')")
    @PostMapping("/add")
    public AjaxResult add(Info info){
        info.setCreateBy(SecurityUtils.getUsername());
        int rows = iInfoService.add(info);
        return toAjax(rows);
    }

    @PreAuthorize("@ss.hasPermi('zmr:info:query')")
    @GetMapping("/{infoId}")
    public AjaxResult selectById(@PathVariable int infoId){
        System.out.println(infoId);
        Info info = iInfoService.findInfoById(infoId);
        return AjaxResult.success(info);
    }

    @PreAuthorize("@ss.hasPermi('zmr:info:edit')")
    @PutMapping("/update")
    public AjaxResult modify(@RequestBody Info info){
        info.setUpdateBy(SecurityUtils.getUsername());
        int rows = iInfoService.modify(info);
        return toAjax(rows);
    }

    @PreAuthorize("@ss.hasPermi('zmr:info:edit')")
    @DeleteMapping("/delete/{infoIds}")
    public AjaxResult remove(@PathVariable int[] infoIds){
        System.out.println(Arrays.toString(infoIds));
        int rows = iInfoService.remove(infoIds);
        return toAjax(rows);
    }

    @PreAuthorize("@ss.hasPermi('zmr:info:edit')")
    @PostMapping("/reDel")
    public AjaxResult reRemove(){
        int rows = iInfoService.reRemove();
        return toAjax(rows);
    }

    @PreAuthorize("@ss.hasPermi('qs:index:query')")
    @GetMapping("/listProductions")
    public AjaxResult listProduction(){
        List<DrugProduction> list = drugService.findOne(null);
        return AjaxResult.success(list);
    }
}
