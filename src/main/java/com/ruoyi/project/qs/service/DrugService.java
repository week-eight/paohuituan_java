package com.ruoyi.project.qs.service;

import com.ruoyi.project.qs.domain.DrugProduction;

import java.util.List;

public interface DrugService {
    //查询厂家
    List<DrugProduction> findOne(DrugProduction drugProduction);

   //新增厂家
    int addAll(DrugProduction drugProduction);

   //主键查询厂家
    DrugProduction getById(Long product_id);
   //厂家修改
    int updateDept(DrugProduction drugProduction);
    //删除
    int removeDepts(Long[] deptIds);
}
