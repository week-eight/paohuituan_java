package com.ruoyi.project.qs.service.impl;

import com.ruoyi.project.qs.domain.DrugProduction;
import com.ruoyi.project.qs.mapper.DrugMapper;
import com.ruoyi.project.qs.service.DrugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@Service
public class DrugServiceImpl  implements DrugService {
    @Autowired
    private DrugMapper drugMapper;
    @Override
    public List<DrugProduction> findOne(DrugProduction drugProduction) {
        return drugMapper.selectOne(drugProduction);
    }

    @Override
    public int addAll(DrugProduction drugProduction) {
        drugProduction.setCreateTime(new Date());
        return drugMapper.insert(drugProduction);
    }

    @Override
    public DrugProduction getById(Long product_id) {
        return drugMapper.selectById(product_id);
    }

    @Override
    public int updateDept(DrugProduction drugProduction) {
        drugProduction.setCreateTime(new Date());
        return drugMapper.update(drugProduction);
    }

    @Override
    public int removeDepts(Long[] deptIds) {
        return drugMapper.batchDelete(deptIds);
    }


}
