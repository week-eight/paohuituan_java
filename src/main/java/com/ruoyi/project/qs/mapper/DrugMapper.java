package com.ruoyi.project.qs.mapper;


import com.ruoyi.project.qs.domain.DrugProduction;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DrugMapper {
    //模糊查询
    List<DrugProduction> selectOne(DrugProduction drugProduction);

     //新增
    int insert(DrugProduction drugProduction);
   //主键查询
    DrugProduction selectById(Long product_id);
    //厂家修改
    int update(DrugProduction drugProduction);
    //删除
    int batchDelete(@Param("deptIds") Long[] deptIds);
}
