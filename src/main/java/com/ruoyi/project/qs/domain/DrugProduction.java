package com.ruoyi.project.qs.domain;





import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode
public class DrugProduction extends BaseEntity {
private int   product_id;
private String production_name;
private String production_cod;
private String production_con;
private String production_phone;
private String production_key;
private String production_state;
  private String production_site;
@JsonFormat(pattern = "yyyy-MM-dd hh:MM:ss")
@DateTimeFormat(pattern = "yyyy-MM-dd hh:MM:ss")
private Date creation_time;

}
