package com.ruoyi.project.qs.controlle;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.qs.domain.DrugProduction;
import com.ruoyi.project.qs.service.DrugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/grup/hisdept")
public class DrugController extends BaseController {
 @Autowired
    private DrugService drugService;
//新增厂家
@PreAuthorize("@ss.hasPermi('qs:index:add')")
    @PostMapping
    public AjaxResult hisDept(@RequestBody DrugProduction drugProduction){
    drugProduction.setCreateBy(SecurityUtils.getUsername());
    int rows=drugService.addAll(drugProduction);
        return toAjax(rows);
    }

//查询厂家
@PreAuthorize("@ss.hasPermi('qs:index:query')")
 @GetMapping("/list")
 public TableDataInfo list(DrugProduction drugProduction){
     List<DrugProduction> all = drugService.findOne(drugProduction);
     return getDataTable(all);
 }
 //主键查询
    @GetMapping("/{product_id}")
    @PreAuthorize("@ss.hasPermi('qs:index:query')")
    public AjaxResult getById(@PathVariable Long product_id){
    DrugProduction drugProduction = drugService.getById(product_id);
    return AjaxResult.success(drugProduction);
    }
    //厂家修改
    @PutMapping
    @PreAuthorize("@ss.hasPermi('qs:index:edit')")
    public  AjaxResult editDept(@RequestBody DrugProduction drugProduction){
       //设置修改人姓名
       drugProduction.setUpdateBy(SecurityUtils.getUsername());
       int rwos = drugService.updateDept(drugProduction);
    return toAjax(rwos);
    }
    //厂家删除
    @DeleteMapping("/{deptIds}")
    @PreAuthorize("@ss.hasPermi('system:hisdept:remove')")
    public AjaxResult removeDepts(@PathVariable Long[] deptIds){

    return toAjax(drugService.removeDepts(deptIds));
    }
}
