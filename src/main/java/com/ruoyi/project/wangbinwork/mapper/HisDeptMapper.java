package com.ruoyi.project.wangbinwork.mapper;


import com.ruoyi.project.wangbinwork.domain.HisDept;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface HisDeptMapper {
    //查询科室
    List<HisDept> selectList(HisDept hisDept);
    //新增科室
    int insert(HisDept hisDept);

    //修改查询到数据，根据主键deptId
    HisDept selectById(Long deptId);
    //修改更新信息
    int update(HisDept hisDept);
    //批量删除
    int bathDelete(@Param("deptIds") Long[] deptIds);
    
    //下拉科室
    List<HisDept> selectAll();
}
