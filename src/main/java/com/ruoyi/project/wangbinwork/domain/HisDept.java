package com.ruoyi.project.wangbinwork.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class HisDept extends BaseEntity {
    private Long deptId; // 主键
    private String deptName; // 科室名称
    private String deptCode; // 科室编码
    private long deptNum; // 挂号量
    private String deptLeader; // 负责人
    private String deptPhone; // 手机号码
    private String status; // 状态
}
