package com.ruoyi.project.wangbinwork.controller;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.wangbinwork.domain.HisDept;
import com.ruoyi.project.wangbinwork.service.IHisDeptService;
import org.apache.catalina.security.SecurityUtil;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/system/hisdept")
public class HisDeptController extends BaseController {
    @Autowired
    private IHisDeptService iHisDeptService;
    
    @GetMapping("/all")
    public AjaxResult getAll() {
        return AjaxResult.success(iHisDeptService.getAll());
    }
    /***
     * 查询科室全部信息
     * @param hisDept
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:hisdept:query')")
    @GetMapping("/list")
    public TableDataInfo list(HisDept hisDept) {
        //分页技术，处理请求参数pageNume，pageSize,并使用pageHelper技术
        startPage();
        //调用servcie查询数据库
        List<HisDept> list = iHisDeptService.selectList(hisDept);
        return getDataTable(list);
    }

    /**
     * 增加科室信息
     *
     * @param hisDept
     * @return
     */
    @PostMapping
    @PreAuthorize("@ss.hasPermi('system:hisdept:add')")
    public AjaxResult addHisDept(@RequestBody HisDept hisDept) {
        hisDept.setCreateBy(SecurityUtils.getUsername());
        int rows = iHisDeptService.addHisDept(hisDept);
        return toAjax(rows);
    }

    /**
     * 修改科室，根据id查询结果
     * @param deptId
     * @return hisdept对象
     */
    @GetMapping("/{deptId}")
    @PreAuthorize("@ss.hasPermi('system:hisdept:query')")
    public AjaxResult getById(@PathVariable Long deptId){
        HisDept hisDept =iHisDeptService.getById(deptId);
        return AjaxResult.success(hisDept);
    }

    /**
     * 修改科室，并保存，确定按钮
     * @param hisDept
     * @return    返回一行数据
     */
    @PutMapping
    @PreAuthorize("@ss.hasPermi('system:hisdept:edit')")
    public AjaxResult editDept(@RequestBody HisDept hisDept){
        //设置修改人姓名
        hisDept.setUpdateBy(SecurityUtils.getUsername());
        int row= iHisDeptService.updateDept(hisDept);
        return toAjax(row);
    }

    /**
     * 更新科室
     * @param deptIds 多个或者单个主键，可以批量删除
     * @return
     */
    @DeleteMapping("/{deptIds}")
    @PreAuthorize("@ss.hasPermi('system:hisdept:remove')")
    public  AjaxResult removeDepts(@PathVariable Long[] deptIds){
        return  toAjax(iHisDeptService.removeDepts(deptIds));
    }

}
