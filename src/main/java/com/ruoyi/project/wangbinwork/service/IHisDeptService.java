package com.ruoyi.project.wangbinwork.service;

import com.ruoyi.project.wangbinwork.domain.HisDept;

import java.util.List;

public interface IHisDeptService {
    /**
     * 根据条件，访问数据库，查询科室
     * 分页逻辑使用pageHelper实现
     * @param hisDept
     * @return
     */
    List<HisDept> selectList(HisDept hisDept);

    /**
     * 新增科室信息
     * @param hisDept
     * @return
     */
    int addHisDept(HisDept hisDept);

    /**
     * 修改科室，根据id先查询到数据
     * @param deptId
     * @return
     */
    HisDept getById(Long deptId);

    /**
     * 修改科室，保存信息
     * @param hisDept
     * @return 一行数据
     */
    int updateDept(HisDept hisDept);

    /**
     * 删除科室，可以单个，可以批量
     * @param deptIds
     * @return
     */
    int removeDepts(Long[] deptIds);
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;下拉科室
     * @return java.util.List<com.ruoyi.project.wangbinwork.domain.HisDept>
     */
    List<HisDept> getAll();
}
