package com.ruoyi.project.wangbinwork.service.impl;

import com.ruoyi.project.wangbinwork.domain.HisDept;
import com.ruoyi.project.wangbinwork.mapper.HisDeptMapper;
import com.ruoyi.project.wangbinwork.service.IHisDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class HisdeptServiceImpl  implements IHisDeptService {
    //调用mapper层
    @Autowired
    private HisDeptMapper hisDeptMapper;
    /**
     * 查询科室
     * @param hisDept 查询条件
     * @return
     */
    @Override
    public List<HisDept> selectList(HisDept hisDept) {
      List<HisDept> list=  hisDeptMapper.selectList(hisDept);
        return list;
    }

    /**
     * 新增科室信息
     * @param hisDept
     * @return
     */
    @Override
    public int addHisDept(HisDept hisDept) {
        //考虑是否有数据需要赋值
        hisDept.setCreateTime(new Date());
        return  hisDeptMapper.insert(hisDept);
    }

    /**
     * 修改时的查询
     * @param deptId 根据主键
     * @return
     */
    @Override
    public HisDept getById(Long deptId) {

        return hisDeptMapper.selectById(deptId);
    }

    /**
     * 修改数据，保存数据
     * @param hisDept
     * @return
     */
    @Override
    public int updateDept(HisDept hisDept) {
        //设置修改时间
        hisDept.setUpdateTime(new Date());
        return hisDeptMapper.update(hisDept);
    }

    /**
     * 批量删除
     * @param deptIds
     * @return
     */

    @Override
    public int removeDepts(Long[] deptIds) {
        return hisDeptMapper.bathDelete(deptIds);
    }
    
    /**
     * <b>Description:</b><br>
     * &emsp;&emsp;&emsp;下拉科室
     * @return java.util.List<com.ruoyi.project.wangbinwork.domain.HisDept>
     */
    @Override
    public List<HisDept> getAll() {
        return hisDeptMapper.selectAll();
    }
}
