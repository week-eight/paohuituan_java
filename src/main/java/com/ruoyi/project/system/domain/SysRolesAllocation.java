package com.ruoyi.project.system.domain;

import com.ruoyi.framework.web.domain.BaseEntity;

import java.util.Arrays;

/**
 * <p>
 *
 * </p>
 * <b>ClassName:</b><br>
 * &emsp;&emsp;&emsp;SysRolesAllocation
 * <br><b>Description:</b><br>
 * &emsp;&emsp;&emsp;角色分配
 * <br><b>Date:</b><br>
 * &emsp;&emsp;&emsp;2021/7/15 22:04
 *
 * @author: week_eight
 * @version: v1.0
 */
public class SysRolesAllocation extends BaseEntity {

    //用户id
    private Long roleUserId;
    //角色id数组
    private Long[] roleIds;
    //角色id
    private Long roleId;
    
    public SysRolesAllocation() {
    }
    
    public SysRolesAllocation(Long roleUserId, Long[] roleIds, Long roleId) {
        this.roleUserId = roleUserId;
        this.roleIds = roleIds;
        this.roleId = roleId;
    }
    
    public Long getRoleUserId() {
        return roleUserId;
    }
    
    public void setRoleUserId(Long roleUserId) {
        this.roleUserId = roleUserId;
    }
    
    public Long[] getRoleIds() {
        return roleIds;
    }
    
    public void setRoleIds(Long[] roleIds) {
        this.roleIds = roleIds;
    }
    
    public Long getRoleId() {
        return roleId;
    }
    
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    
    @Override
    public String toString() {
        return "SysRolesAllocation{" +
                "roleUserId=" + roleUserId +
                ", roleIds=" + Arrays.toString(roleIds) +
                ", roleId=" + roleId +
                '}';
    }
}
