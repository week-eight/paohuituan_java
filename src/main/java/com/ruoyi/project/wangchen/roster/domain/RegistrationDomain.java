package com.ruoyi.project.wangchen.roster.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @date : 8:39 2021/7/15
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RegistrationDomain extends BaseEntity {

    //挂号表ID
    private Long registrationId;
    //挂号名称
    private String registrationName;

}
