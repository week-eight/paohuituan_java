package com.ruoyi.project.wangchen.roster.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.wangchen.inspect.domain.InspectDoMain;
import com.ruoyi.project.wangchen.roster.domain.RosterDomain;
import com.ruoyi.project.wangchen.roster.domain.UserDomain;
import com.ruoyi.project.wangchen.roster.service.RosterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @date : 16:32 2021/7/14
 */
@RestController
@RequestMapping("/system/roster")
public class RosterController extends BaseController {

    @Autowired
    private RosterService rosterService;


    /**
     * 查询全部排班信息
     * @param
     * @return 分页查询用TableDataInfo
     *         不分页查询用AjaxResult
     */
    @PreAuthorize("@ss.hasPermi('wangchen:invalid:index')")
    @GetMapping("/list")
    public AjaxResult list(RosterDomain rosterDomain) {


        System.out.println(rosterDomain);

      /*  //获取医生姓名
        String nikeName = rosterDomain.getUserDomain().getNikeName();
        //获取部门名称
        String deptName = rosterDomain.getUserDomain().getDeptName();*/

        //调用service接口查询数据库
        List<UserDomain> list = rosterService.selectRoster(rosterDomain);

        return AjaxResult.success(list);
    }

}
