package com.ruoyi.project.wangchen.roster.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @date : 16:19 2021/7/14
 */


@Data
@AllArgsConstructor
@NoArgsConstructor
public class RosterDomain extends BaseEntity {

    //医生id 通过ID (sys_user表中的部门deptid  再通过deptid获得科室)获得科室信息
   private Long treatmentDoctorid;
   //排班日期
   private Date treatmentDate;
   //排班时段  早 中 晚
   private String  treatmentTime;
   //通过挂号费用Id 获得挂号类型
   private  Long treatmentRegid;
   // 挂号类型
   private String registrationName;
  /* //注入挂号表对象  获取对象内挂号名称
   private  RegistrationDomain registrationDomain;
   //用户信息表
   private UserDomain userDomain;*/
   //医生姓名
   private String nickName;
   //科室姓名
   private String deptName;

    //用户Id
    private Long userId;

    //科室ID
    private Long deptId;
}
