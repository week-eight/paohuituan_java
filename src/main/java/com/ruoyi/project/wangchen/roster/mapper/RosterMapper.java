package com.ruoyi.project.wangchen.roster.mapper;

import com.ruoyi.project.wangchen.roster.domain.RosterDomain;
import com.ruoyi.project.wangchen.roster.domain.UserDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @date : 9:15 2021/7/15
 */
@Mapper
public interface RosterMapper {

    /**
     * 排班信息
     * @param
     * @return
     */
    List<UserDomain> rosterList(RosterDomain rosterDomain);

}
