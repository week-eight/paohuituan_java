package com.ruoyi.project.wangchen.roster.service;

import com.ruoyi.project.wangchen.roster.domain.RosterDomain;
import com.ruoyi.project.wangchen.roster.domain.UserDomain;

import java.util.List;

/**
 * @date : 16:34 2021/7/14
 */
public interface RosterService {

    /**
     * 查询医生排班信息
     * @param rosterDomain
     * @return
     */
    List<UserDomain> selectRoster(RosterDomain rosterDomain);


}
