package com.ruoyi.project.wangchen.roster.service.impl;

import com.ruoyi.project.wangchen.roster.domain.RosterDomain;
import com.ruoyi.project.wangchen.roster.domain.UserDomain;
import com.ruoyi.project.wangchen.roster.mapper.RosterMapper;
import com.ruoyi.project.wangchen.roster.service.RosterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @date : 16:34 2021/7/14
 */
@Service
public class IrosterService implements RosterService {

    @Autowired
    private RosterMapper rosterMapper;


    /**
     * 医生排班查询展示  将排班日期转为星期展示
     * @param rosterDomain
     * @return
     */
    @Override
    public List<UserDomain> selectRoster(RosterDomain rosterDomain) {

        List<UserDomain> list = rosterMapper.rosterList(rosterDomain);


        return list ;
    }




    /*public static String dateToWeek(Date date) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        String[] weekDays = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
        Calendar cal = Calendar.getInstance(); // 获得一个日历
        Date datet = null;
        try {
            datet = f.parse(String.valueOf(date));
            cal.setTime(datet);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1; // 指示一个星期中的某天。
        if (w < 0)
            w = 0;
        return weekDays[w];
    }*/
}
