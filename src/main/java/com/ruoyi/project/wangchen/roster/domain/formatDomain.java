package com.ruoyi.project.wangchen.roster.domain;

import java.util.Date;

/**
 * @date : 13:11 2021/7/16
 */
public class formatDomain {


    //排班日期
    private Date treatmentDate;
    //排班时段  早 中 晚
    private String  treatmentTime;
    //用户Id
    private Long userId;
    //医生姓名
    private String nickName;
    //科室姓名
    private String deptName;
    //科室ID
    private Long deptId;

    private String week1;
    private String week2;
    private String week3;
    private String week4;
    private String week5;
    private String week6;
    private String week7;

}
