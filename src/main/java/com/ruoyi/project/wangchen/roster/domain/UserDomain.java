package com.ruoyi.project.wangchen.roster.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @date : 8:46 2021/7/15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDomain  extends BaseEntity {

    //用户Id
    private Long userId;
    //医生姓名
    private String nickName;
    //科室ID
    private Long deptId;
    //科室姓名
    private String deptName;

}
