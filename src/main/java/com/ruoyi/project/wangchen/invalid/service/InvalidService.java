package com.ruoyi.project.wangchen.invalid.service;

import com.ruoyi.project.wangchen.invalid.domain.InvalidDomain;

import java.util.List;

/**
 * @date : 17:58 2021/7/16
 */


public interface InvalidService {

    /**
     * 患者库查询接口
     * @param invalidDomain
     * @return
     */
    List<InvalidDomain> invalidList(InvalidDomain invalidDomain);
}
