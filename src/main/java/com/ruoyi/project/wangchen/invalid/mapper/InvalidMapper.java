package com.ruoyi.project.wangchen.invalid.mapper;

import com.ruoyi.project.wangchen.invalid.domain.InvalidDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @date : 18:50 2021/7/16
 */
@Mapper
public interface InvalidMapper {


    /**
     * 查询患者接口
     * @param invalidDomain
     * @return
     */
    List<InvalidDomain> invalidList(InvalidDomain invalidDomain);
}
