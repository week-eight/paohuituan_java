package com.ruoyi.project.wangchen.invalid.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @date : 17:13 2021/7/16
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class InvalidDomain extends BaseEntity {

    //流水号
    private int patientId;
   //患者姓名
    private String patientName;
    //患者电话
    private  String patientPhone;
    //患者身份证号
    private  String patientIdentity;
    // 患者出生日期
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date patientBirth;
    //患者年龄
    private  int patientAge;
    //患者性别
    private String patientSex;
    //患者信息状态
    private String patientState;
    //患者创建时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private  Date patientStart;
   // 患者地址信息
    private String patientAddress;
    //患者过敏史
    private String patientAllergy;

}
