package com.ruoyi.project.wangchen.invalid.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.wangchen.inspect.domain.InspectDoMain;
import com.ruoyi.project.wangchen.inspect.service.IInspectService;
import com.ruoyi.project.wangchen.inspect.service.impl.InspectService;
import com.ruoyi.project.wangchen.invalid.domain.InvalidDomain;
import com.ruoyi.project.wangchen.invalid.service.InvalidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @date : 17:56 2021/7/16
 */
@RestController
@RequestMapping("/system/invalid")
public class InvalidController extends BaseController {

    @Autowired
    private InvalidService invalidService;

    /**
     * 获取患者资料
     * @param
     * @return AjaxResult:专门处理不分页相关逻辑
     *         TableDataInfo:专门处理分页相关逻辑
     */
    @PreAuthorize("@ss.hasPermi('wangchen:invalid:index')")
    @GetMapping("/list")
    public TableDataInfo list(InvalidDomain invalidDomain)
    {
        //类继承父控制层类 调用分页相关逻辑 处理请求参数pageNum pageSize等 使用PageHelper处理分页
        startPage();
        //调用service 查询数据库
      List<InvalidDomain> list =  invalidService.invalidList(invalidDomain);
        return getDataTable(list);
    }



}
