package com.ruoyi.project.wangchen.invalid.service.impl;

import com.ruoyi.project.wangchen.inspect.mapper.InspectMapper;
import com.ruoyi.project.wangchen.invalid.domain.InvalidDomain;
import com.ruoyi.project.wangchen.invalid.mapper.InvalidMapper;
import com.ruoyi.project.wangchen.invalid.service.InvalidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @date : 17:59 2021/7/16
 */
@Service
public class InvalidServiceImpl implements InvalidService {


    @Autowired
    private InvalidMapper invalidMapper;
    /**
     * 查询患者库
     * @param invalidDomain
     * @return
     */
    @Override
    public List<InvalidDomain> invalidList(InvalidDomain invalidDomain) {
        return invalidMapper.invalidList(invalidDomain);
    }
}
