package com.ruoyi.project.wangchen.feequrey.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.wangchen.feequrey.domain.FeequeryDoMain;
import com.ruoyi.project.wangchen.feequrey.service.impl.IFeequeryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * @date : 14:46 2021/7/17
 */
@RestController
@RequestMapping("/system/feequrey")
public class FeequeryController extends BaseController {
    
    // 应用标识
    private final String APP_ID = "2021000117691204";
    // 应用私钥
    private final String APP_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCFdHtbDbqy1cC5gKRFqoeQu+qd5j1iAN2FPND3LucASlCl1u+nGlbfbXZWVlhxADLzazF2i1DV5kjJUFx+N9mDpUqldKRMRDBAs+z9o3bwyDSswS2YsGwBrIVsWjo2rP0XVP8j19nppwYVx0uoAag9t2E3ORo2S02G7UpGFr5GB3ILEA/8UIrxXQNtYgeAR79j/+45KeQP9CHGG+qOP4r35oeeUtwz4KbrTzkpixUJwE2pvIAVhW4my+l9u2jtK4Hjl36+Y47zmOCo6uva0WzUMLtPUaRi26fEwkvAD6FZV3Ni8SMTs1TK1bYltavgzOLYvN8jV+oL2lOu31Uld12DAgMBAAECggEAGYu3hTF2mH8EOj4es9s1wgXGnl0RSO5DeRqwIXDP9SVElMo067bGBFTOvy4eFdtk5WWSfU+jtJ41Npv0aJDGSUXdqRhPzu2KkwgeCFs/DE42jw8XbE/xDMz6aUhINmkctVem0zShWPGI36hbmhhfHRk6ObjrhJMbjwVjH1GBMKQ5T7649JznN4so0DqBkMkC+QPXCuKBwSyDGg2qcPTE7sfyOEZpOIdrWeOcNxb8cSD0dfmqcBoh4C2Z5H2vO0qaOj6OeDblRWWQ2057W7/K+6mN4jjTuhjzKZM9Z3J5bbjzPN9OLPNCwVKIZSWII8zT10HHQNX6ZPIMJ0X1bhefiQKBgQDsPoPKf6lZcLa+O8jeeFso2Ix3ghBM1C2t2tkYdFaMPOtwHSn5QHjD4M61Ch4gegjV/IlEIvAjoLVaywb2FjvNErgdR69n2sxIMuUWcThQasVOxmfQYwflg60t2yBMBYWqQSTdz1RAxYa3RCygqxhpaCg7aCGiVfP1BmrzDsemPwKBgQCQnXgoHXWhHf8iKw/JpRViu2PKU5DqRjKPnpkxBQzkdLk9alUTtAfLgYi84SQIK3OFqA4bMyiBGtzI+i1c7GhMYN2SA/iBNMem6VVNtE2nL/bZxowKbNM5Eh5sLh1+1I58WImfMYb6n4PQQjPcbmUe5O0SMyLzT4gcJYYklRAfvQKBgQCZ2NBMtDxd0ahAVICoWGrNO9CmFx50WgzxVj7gzQp7w0Rzu5pVBqZesW0Luu4Jqk9Mw2vE4pfOK99OpD8BTVNeZl1ePAu/r14vem1z4wGQZSfe+BgtPgCrd8f0v+nVeBcm28udY2Jz1+62NIB3lVSNeFYS8epNZHo9zBMj/DEKUwKBgEiMlzEYxmBT2iDcFYmFphroA4IHNlYxbUAbwqKS9ArQe/XRmGjQEnEHxP9su1TV55iP3LPMkG9keIri50sM1q4qkj2kfFDll5umx0K6R5KuP1q1fp/kRHsHGfgPkoCERg8fo7sXb63fxJMAA2B8iYJcOoq82LXTmmb0dD16AqLJAoGAcSeVfyvvYU14/YO+R2zcnvG0Tnkda7j7KwUf/5m7mAlFfTszziycOOvABwdyJFco4SJxXCY7L/0RgBEyIvYlgHg0+pQHR+SMelnxqkBfqG/G2ob3EQY0NPCO3vIRt9aDd14vmSVrpj5UkuGivEYgTe7JuloU9yDRkL7vVRD/OCQ=";
    private final String CHARSET = "UTF-8";
    // 支付宝公钥
    private final String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgeXoMmadQO0THBJQxJv4wB2zrSGubOmneRmFqFS1BgZXT1j1Kw66wW9C75Mq/+GAMqELl8751px0zV7bRr7j0VFM7EEDg39RqFyu3KTAGivd9XlPwq6MsQDYVigtY2kRwSHUBRAm4QRHahQivvOvH0ieXAWClPE7XCHU/5+E2S0/C2S4FRtQYn8+7/0AkrC/TtxyqG8OhoF3drrkKk6pJzzNOqcavJuIHB/CHYEaQJFMPd9zJQ9BtwLrjJqVULp2WvCT2QINlbtRuop7B8GPZzBbqVRHXstv88xV5+c2qxLy3pzRVDGpTmT2rU1DjQPYrYhsbQVDf4M/WbfOZ6XGLQIDAQAB";
    // 沙箱路径接口，正是路径应为https://openapi.alipay.com/gateway.do
    private final String GATEWAY_URL = "https://openapi.alipaydev.com/gateway.do";
    // 参数返回格式
    private final String FORMAT = "JSON";
    // 签名方式
    private final String SIGN_TYPE = "RSA2";
    // 支付宝异步通知路径，付款完毕后会异步调用本项目的方法，必须为公网地址
    /*private final String NOTIFY_URL = "http://127.0.0.1/notifyURL";*/
    // 支付宝同步通知路径，也就是当付款完毕后跳转本项目的页面，可以不是公网地址
    private final String RETURN_URL = "http://localhost";
    
    
    
    @Autowired
    private IFeequeryService iFeequeryService;

    /**
     * 支付收费查询
     * @param feequeryDoMain
     * @return 分页集合
     */
    @GetMapping("/list")
    public TableDataInfo list(FeequeryDoMain feequeryDoMain) {
        System.out.println(feequeryDoMain);
        startPage();
      List<FeequeryDoMain> list = iFeequeryService.selectFeequery(feequeryDoMain);
        return  getDataTable(list);
    }
    
//    /**
//     * 支付收费查询
//     * @return 分页集合
//     */
//    @PreAuthorize("@ss.hasPermi('wangchen:feequrey:index')")
//    @GetMapping("/pay/{treatmenTregistrationId}")
//    public TableDataInfo pay(@PathVariable String treatmenTregistrationId) {
//        FeequeryDoMain feequeryDoMain = new FeequeryDoMain();
//        feequeryDoMain.setTreatmenTregistrationId(treatmenTregistrationId);
//        List<FeequeryDoMain> list = iFeequeryService.selectFeequery(feequeryDoMain);
//        String payprice = null;
//        for (FeequeryDoMain f : list) {
//            payprice = f.getTotalAmount();
//        }
//        double money = Double.parseDouble(payprice);
//        System.out.println(money);
//        return  null;
//    }
    
    
    /**
     * 支付收费查询
     * @return 分页集合
     */
    @RequestMapping("/pay")
    public void alipay( String totalAmount, HttpServletResponse httpResponse) throws IOException {
        /*FeequeryDoMain feequeryDoMain = new FeequeryDoMain();
        feequeryDoMain.setTreatmenTregistrationId(treatmenTregistrationId);
        List<FeequeryDoMain> list = iFeequeryService.selectFeequery(feequeryDoMain);
        String payprice = null;
        for (FeequeryDoMain f : list) {
            payprice = f.getTotalAmount();
        }
        System.out.println(payprice);*/
    
        // 实例化客户端,填入所需参数
        // 网关地址 应用标识 应用私钥 传输格式 编码格式 支付宝公钥 签名类型
        AlipayClient alipayClient = new DefaultAlipayClient(GATEWAY_URL, APP_ID, APP_PRIVATE_KEY, FORMAT, CHARSET, ALIPAY_PUBLIC_KEY, SIGN_TYPE);
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
        //在公共参数中设置回跳和通知地址
        request.setReturnUrl(RETURN_URL);
//        request.setNotifyUrl(NOTIFY_URL);​
        //商户订单号，商户网站订单系统中唯一订单号，必填
        //生成随机Id
        String out_trade_no = UUID.randomUUID().toString();
        //付款金额，必填
        String total_amount =totalAmount;
        System.out.println(total_amount);
        //订单名称，必填
        String subject ="奥迪A8 2016款 A8L 60 TFSl quattro豪华型";
        //商品描述，可空
        String body = "尊敬的会员欢迎购买奥迪A8 2016款 A8L 60 TFSl quattro豪华型";
        request.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        String form = "";
        try {
            form = alipayClient.pageExecute(request).getBody(); // 调用SDK生成表单
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        httpResponse.setContentType("text/html;charset=" + CHARSET);
        httpResponse.getWriter().write(form);// 直接将完整的表单html输出到页面
        httpResponse.getWriter().flush();
        httpResponse.getWriter().close();
    }
}
