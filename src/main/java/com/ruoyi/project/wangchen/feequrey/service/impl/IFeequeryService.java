package com.ruoyi.project.wangchen.feequrey.service.impl;

import com.ruoyi.project.wangchen.feequrey.domain.FeequeryDoMain;
import com.ruoyi.project.wangchen.feequrey.mapper.FeequeryMapper;
import com.ruoyi.project.wangchen.feequrey.service.FeequeryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @date : 14:48 2021/7/17
 */
@Service
public class IFeequeryService implements FeequeryService {

    @Autowired
    private FeequeryMapper feequeryMapper;

    /**
     * 分页查询收费
     * @param feequeryDoMain
     * @return
     */
    @Override
    public List<FeequeryDoMain> selectFeequery(FeequeryDoMain feequeryDoMain) {


        return feequeryMapper.selectFeequery(feequeryDoMain);

    }
}
