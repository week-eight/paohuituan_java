package com.ruoyi.project.wangchen.feequrey.service;

import com.ruoyi.project.wangchen.feequrey.domain.FeequeryDoMain;

import java.util.List;

/**
 * @date : 14:47 2021/7/17
 */
public interface FeequeryService {

    /**
     * 分页查询收费
     * @param feequeryDoMain
     * @return
     */
    List<FeequeryDoMain> selectFeequery(FeequeryDoMain feequeryDoMain);
}
