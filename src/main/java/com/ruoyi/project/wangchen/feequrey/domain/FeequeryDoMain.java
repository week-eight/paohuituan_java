package com.ruoyi.project.wangchen.feequrey.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @date : 14:16 2021/7/17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FeequeryDoMain extends BaseEntity {

    //患者身份证号
    private String treatmentIdentity;
    //病例编号
    //private String treatmentCaseid;
    //患者姓名
    private String patientName;
    //挂号单ID
    private String treatmenTregistrationId;
    //订单ID
    private  String treatmentOrderid;
    //总金额
    private  String  totalAmount;
    //支付类型
    private String treatmentPayType;
    //订单状态
    private String treatmentPaystatus;

}
