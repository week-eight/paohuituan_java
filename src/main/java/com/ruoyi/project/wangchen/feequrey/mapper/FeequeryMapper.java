package com.ruoyi.project.wangchen.feequrey.mapper;

import com.ruoyi.project.wangchen.feequrey.domain.FeequeryDoMain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @date : 14:48 2021/7/17
 */
@Mapper
public interface FeequeryMapper {

    /**
     * 分页查询收费
     * @param feequeryDoMain
     * @return
     */
    List<FeequeryDoMain> selectFeequery(FeequeryDoMain feequeryDoMain);
}
