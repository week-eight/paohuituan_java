package com.ruoyi.project.wangchen.inspect.controller;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.domain.SysDept;
import com.ruoyi.project.wangchen.inspect.domain.InspectDoMain;
import com.ruoyi.project.wangchen.inspect.service.IInspectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @date : 15:37 2021/7/13
 */

/**
 * 检查费用模块
 */
@RestController
@RequestMapping("/system/inspect")
public class InspectController extends BaseController {

    @Autowired
    private IInspectService inspectService;

    @DeleteMapping("/{inspectId}")
    @PreAuthorize("@ss.hasPermi('system:inspect:remove')")
    public  AjaxResult removeDepts(@PathVariable Long[] inspectId){
         return toAjax(inspectService.removeInspects(inspectId));
    }


    @PutMapping
    @PreAuthorize("@ss.hasPermi('system:inspect:edit')")
    public  AjaxResult editInspect(@RequestBody InspectDoMain inspectDoMain){
        //设置修改人姓名
        inspectDoMain.setCreateBy(SecurityUtils.getUsername());
         int rows = inspectService.updateInspect(inspectDoMain);
        return toAjax(rows);
    }


    /**
     * 修改数据回显
     * @param inspectId
     * @return
     */

    @GetMapping("/{inspectId}")
    @PreAuthorize("@ss.hasPermi('system:inspect:query')")
    public AjaxResult getById(@PathVariable Long inspectId){
          InspectDoMain inspectDoMain =  inspectService.getById(inspectId);
          return AjaxResult.success(inspectDoMain);
    }




    /**
     * 新增检查项目
     * @param inspectDoMain
     * @return 新增行数
     */
    @PreAuthorize("@ss.hasPermi('system:inspect:add')")
    @PostMapping
    public  AjaxResult addInspect(@RequestBody InspectDoMain inspectDoMain){
        inspectDoMain.setCreateBy(SecurityUtils.getUsername());
       int rows = inspectService.addInspect(inspectDoMain);
        return toAjax(rows);
    }



    /**
     * 获取检查费用列表
     * @param inspectDoMain
     * @return AjaxResult:专门处理不分页相关逻辑
     *         TableDataInfo:专门处理分页相关逻辑
     */
    @PreAuthorize("@ss.hasPermi('system:inspect:query')")
    @GetMapping("/list")
    public  TableDataInfo list(InspectDoMain inspectDoMain)
    {
        //类继承父控制层类 调用分页相关逻辑 处理请求参数pageNum pageSize等 使用PageHelper处理分页
        startPage();
        //调用service 查询数据库
      List<InspectDoMain> list= inspectService.selectList(inspectDoMain);
        return getDataTable(list);
    }

}
