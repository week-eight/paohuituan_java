package com.ruoyi.project.wangchen.inspect.service.impl;

import com.ruoyi.project.wangchen.inspect.domain.InspectDoMain;
import com.ruoyi.project.wangchen.inspect.mapper.InspectMapper;
import com.ruoyi.project.wangchen.inspect.service.IInspectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @date : 16:10 2021/7/13
 */
@Service
public class InspectService implements IInspectService {

    @Autowired
    private InspectMapper inspectMapper;
    //检查费用查询
    @Override
    public List<InspectDoMain> selectList(InspectDoMain inspectDoMain) {
     List<InspectDoMain> list=   inspectMapper.selectList(inspectDoMain);
        return list;
    }

    /**
     * 新增检查项目
     * @param inspectDoMain
     * @return 增加的行数
     */
    @Override
    public int addInspect(InspectDoMain inspectDoMain) {
        //创建时间
        inspectDoMain.setCreateTime(new Date());
        return inspectMapper.insert(inspectDoMain);
    }

    /**
     * 主键查询
     * @param inspectId
     * @return
     */
    @Override
    public InspectDoMain getById(Long inspectId) {
        return inspectMapper.selectById(inspectId);
    }

    /**
     * 修改
     * @param inspectDoMain
     * @return
     */
    @Override
    public int updateInspect(InspectDoMain inspectDoMain) {
        inspectDoMain.setUpdateTime(new Date());
        return inspectMapper.updateInspect(inspectDoMain);
    }

    /**
     * 删除
     * @param inspectId
     * @return
     */
    @Override
    public int removeInspects(Long[] inspectId) {
        return inspectMapper.batchDelete(inspectId);

    }
}
