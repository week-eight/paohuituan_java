package com.ruoyi.project.wangchen.inspect.service;

import com.ruoyi.project.wangchen.inspect.domain.InspectDoMain;

import java.util.List;

/**
 * @date : 16:10 2021/7/13
 */
public interface IInspectService {

    /**
     * 根据查询条件访问数据库查询检查费用
     * 分页用pagehelper
     * @param inspectDoMain
     * @return
     */
    List<InspectDoMain> selectList(InspectDoMain inspectDoMain);

    /**
     * 新增检查项目
     * @param inspectDoMain
     * @return
     */
    int addInspect(InspectDoMain inspectDoMain);

    /**
     * 主键查询数据回显
     * @param inspectId
     * @return
     */
    InspectDoMain getById(Long inspectId);

    /**
     * 修改
     * @param inspectDoMain
     * @return
     */
    int updateInspect(InspectDoMain inspectDoMain);

    /**
     * 删除
     * @param inspectId
     * @return
     */
    int removeInspects(Long[] inspectId);
}
