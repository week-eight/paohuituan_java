package com.ruoyi.project.wangchen.inspect.domain;

import com.alibaba.druid.sql.visitor.functions.Char;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @date : 15:29 2021/7/13
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class InspectDoMain extends BaseEntity {

    private Long inspectId;  //项目费用ID
    private String inspectName; //项目名称
    private String inspectKey; //关键字
    private Double inspectPrice; //项目单价
    private Double inspectCost; //项目成本
    private String inspectUnit; //单位
    private String inspectType; //类型
    private String inspectStatus; //状态（0正常 1停用）

}
