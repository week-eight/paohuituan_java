package com.ruoyi.project.wangchen.inspect.mapper;

import com.ruoyi.project.wangchen.inspect.domain.InspectDoMain;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @date : 16:17 2021/7/13
 */

public interface InspectMapper {

    //查询检查费用
    List<InspectDoMain> selectList(InspectDoMain inspectDoMain);

    //添加检查项目
    int insert(InspectDoMain inspectDoMain);

    //主键查询检查项目
    InspectDoMain selectById(Long inspectId);

    /**
     * 修改
     * @param inspectDoMain
     * @return
     */
    int updateInspect(InspectDoMain inspectDoMain);

    /**
     * 删除
     * @param inspectId
     * @return
     */
    int batchDelete(@Param("inspectId") Long[] inspectId);
}
